#!/bin/bash

AB_HOST_NAME=ansibibble.ask
ASK_PASS="--ask-pass"
ASK_BECOME="--ask-become-pass"
VERBOSITY=""    # "-vvv"
# I need this because I am not a clevel man.
#                                                      And this is why  V
ansible-playbook "$ASK_PASS" "$ASK_BECOME" $VERBOSITY -i ${AB_HOST_NAME}, Ansibibble.yml


# To-do: Prompt to install packages, ansible,sshpass
